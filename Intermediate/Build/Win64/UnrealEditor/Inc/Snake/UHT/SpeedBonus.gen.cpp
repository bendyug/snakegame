// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SpeedBonus.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedBonus() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Snake();
// End Cross Module References
	void ASpeedBonus::StaticRegisterNativesASpeedBonus()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASpeedBonus);
	UClass* Z_Construct_UClass_ASpeedBonus_NoRegister()
	{
		return ASpeedBonus::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedBonus.h" },
		{ "ModuleRelativePath", "SpeedBonus.h" },
	};
#endif
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedBonus, IInteractable), false },  // 478065194
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedBonus>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASpeedBonus_Statics::ClassParams = {
		&ASpeedBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedBonus()
	{
		if (!Z_Registration_Info_UClass_ASpeedBonus.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASpeedBonus.OuterSingleton, Z_Construct_UClass_ASpeedBonus_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASpeedBonus.OuterSingleton;
	}
	template<> SNAKE_API UClass* StaticClass<ASpeedBonus>()
	{
		return ASpeedBonus::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedBonus);
	ASpeedBonus::~ASpeedBonus() {}
	struct Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedBonus_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedBonus_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASpeedBonus, ASpeedBonus::StaticClass, TEXT("ASpeedBonus"), &Z_Registration_Info_UClass_ASpeedBonus, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASpeedBonus), 3246884463U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedBonus_h_812641104(TEXT("/Script/Snake"),
		Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedBonus_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedBonus_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
