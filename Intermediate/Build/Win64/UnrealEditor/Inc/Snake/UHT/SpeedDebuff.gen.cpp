// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SpeedDebuff.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedDebuff() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedDebuff();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedDebuff_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
	UPackage* Z_Construct_UPackage__Script_Snake();
// End Cross Module References
	void ASpeedDebuff::StaticRegisterNativesASpeedDebuff()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASpeedDebuff);
	UClass* Z_Construct_UClass_ASpeedDebuff_NoRegister()
	{
		return ASpeedDebuff::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedDebuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedDebuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedDebuff_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedDebuff.h" },
		{ "ModuleRelativePath", "SpeedDebuff.h" },
	};
#endif
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedDebuff_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedDebuff, IInteractable), false },  // 478065194
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedDebuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedDebuff>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASpeedDebuff_Statics::ClassParams = {
		&ASpeedDebuff::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedDebuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedDebuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedDebuff()
	{
		if (!Z_Registration_Info_UClass_ASpeedDebuff.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASpeedDebuff.OuterSingleton, Z_Construct_UClass_ASpeedDebuff_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASpeedDebuff.OuterSingleton;
	}
	template<> SNAKE_API UClass* StaticClass<ASpeedDebuff>()
	{
		return ASpeedDebuff::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedDebuff);
	ASpeedDebuff::~ASpeedDebuff() {}
	struct Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedDebuff_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedDebuff_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASpeedDebuff, ASpeedDebuff::StaticClass, TEXT("ASpeedDebuff"), &Z_Registration_Info_UClass_ASpeedDebuff, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASpeedDebuff), 70390323U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedDebuff_h_591746467(TEXT("/Script/Snake"),
		Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedDebuff_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Snake_Source_Snake_SpeedDebuff_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
