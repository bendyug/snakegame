// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "SpeedDebuff.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_SpeedDebuff_generated_h
#error "SpeedDebuff.generated.h already included, missing '#pragma once' in SpeedDebuff.h"
#endif
#define SNAKE_SpeedDebuff_generated_h

#define FID_Snake_Source_Snake_SpeedDebuff_h_13_SPARSE_DATA
#define FID_Snake_Source_Snake_SpeedDebuff_h_13_RPC_WRAPPERS
#define FID_Snake_Source_Snake_SpeedDebuff_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Snake_Source_Snake_SpeedDebuff_h_13_ACCESSORS
#define FID_Snake_Source_Snake_SpeedDebuff_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeedDebuff(); \
	friend struct Z_Construct_UClass_ASpeedDebuff_Statics; \
public: \
	DECLARE_CLASS(ASpeedDebuff, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASpeedDebuff) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedDebuff*>(this); }


#define FID_Snake_Source_Snake_SpeedDebuff_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpeedDebuff(); \
	friend struct Z_Construct_UClass_ASpeedDebuff_Statics; \
public: \
	DECLARE_CLASS(ASpeedDebuff, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASpeedDebuff) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedDebuff*>(this); }


#define FID_Snake_Source_Snake_SpeedDebuff_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeedDebuff(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeedDebuff) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedDebuff); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedDebuff(ASpeedDebuff&&); \
	NO_API ASpeedDebuff(const ASpeedDebuff&); \
public: \
	NO_API virtual ~ASpeedDebuff();


#define FID_Snake_Source_Snake_SpeedDebuff_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedDebuff(ASpeedDebuff&&); \
	NO_API ASpeedDebuff(const ASpeedDebuff&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedDebuff); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedDebuff); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeedDebuff) \
	NO_API virtual ~ASpeedDebuff();


#define FID_Snake_Source_Snake_SpeedDebuff_h_10_PROLOG
#define FID_Snake_Source_Snake_SpeedDebuff_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_SPARSE_DATA \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_RPC_WRAPPERS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_ACCESSORS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_INCLASS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Snake_Source_Snake_SpeedDebuff_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_SPARSE_DATA \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_ACCESSORS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_INCLASS_NO_PURE_DECLS \
	FID_Snake_Source_Snake_SpeedDebuff_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class ASpeedDebuff>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Snake_Source_Snake_SpeedDebuff_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
